import {Component} from 'angular2/core';
import {Condition} from './condition';
import DateTimeFormat = Intl.DateTimeFormat;

@Component({
    selector: 'condition-detail',
    template: `
    <div class="col-sm-3">
        <div class="form-group">
            operator
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <input [(ngModel)]="condition.value" />
        </div>
    </div>
    `
})

export class DateCondition extends Condition {
    public name: string = 'date';
//    public operator: Operator[] = ['equals', 'not equals'];
    public value: string;
}

