System.register(['angular2/core', './condition.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, condition_service_1;
    var ConditionBuilderComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (condition_service_1_1) {
                condition_service_1 = condition_service_1_1;
            }],
        execute: function() {
            ConditionBuilderComponent = (function () {
                function ConditionBuilderComponent(_conditionService) {
                    this._conditionService = _conditionService;
                    this.conditions = [];
                }
                ConditionBuilderComponent.prototype.getConditions = function () {
                    var _this = this;
                    this._conditionService.getConditions().then(function (conditions) { return _this.conditions = conditions; });
                };
                ConditionBuilderComponent.prototype.ngOnInit = function () {
                    this.getConditions();
                };
                ConditionBuilderComponent = __decorate([
                    core_1.Component({
                        selector: 'condition-builder',
                        templateUrl: 'app/conditionbuilder.component.html',
                        providers: [condition_service_1.ConditionService]
                    }), 
                    __metadata('design:paramtypes', [condition_service_1.ConditionService])
                ], ConditionBuilderComponent);
                return ConditionBuilderComponent;
            }());
            exports_1("ConditionBuilderComponent", ConditionBuilderComponent);
        }
    }
});
//# sourceMappingURL=form.component.js.map