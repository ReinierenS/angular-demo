System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var CONDITIONS;
    return {
        setters:[],
        execute: function() {
            exports_1("CONDITIONS", CONDITIONS = [
                { "condition": "Auto" },
                { "condition": "Fiets" },
                { "condition": "Boot" },
                { "condition": "Vliegtuig" }
            ]);
        }
    }
});
//# sourceMappingURL=mock-arrays.js.map