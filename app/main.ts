import {bootstrap}    from 'angular2/platform/browser';
import {ConditionBuilderComponent} from 'app/conditionbuilder.component.ts';

bootstrap(ConditionBuilderComponent);