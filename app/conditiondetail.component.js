System.register(['angular2/core', 'angular2/common'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var core_1, common_1;
    var MyComponentLoader, ConditionDetailComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            }],
        execute: function() {
            MyComponentLoader = (function () {
                function MyComponentLoader() {
                }
                MyComponentLoader.prototype.loadComponentConfig = function (url) {
                    var _this = this;
                    return fetch(url)
                        .then(function (res) { return res.json(); })
                        .then(function (componentList) {
                        return Promise.all(componentList.map(function (config) {
                            return _this.loadComponent(config);
                        }));
                    });
                };
                MyComponentLoader.prototype.loadComponent = function (configObject) {
                    return System.import(configObject.path)
                        .then(function (componentModule) {
                        return componentModule[configObject.component];
                    });
                };
                return MyComponentLoader;
            }());
            ConditionDetailComponent = (function () {
                function ConditionDetailComponent(myLoader, loader, elementRef, configPath) {
                    myLoader.loadComponentConfig(configPath)
                        .then(function (components) {
                        return Promise.all(components.map(function (comp) {
                            return loader.loadIntoLocation(comp, elementRef, 'content');
                        }));
                    });
                }
                ConditionDetailComponent = __decorate([
                    core_1.Component({
                        selector: 'condition-detail',
                        bindings: [MyComponentLoader],
                        directives: [common_1.CORE_DIRECTIVES],
                        template: "\n    <div id=\"content\"></div>\n    "
                    }),
                    __param(3, core_1.Attribute('src')), 
                    __metadata('design:paramtypes', [MyComponentLoader, core_1.DynamicComponentLoader, core_1.ElementRef, Object])
                ], ConditionDetailComponent);
                return ConditionDetailComponent;
            }());
            exports_1("ConditionDetailComponent", ConditionDetailComponent);
        }
    }
});
//# sourceMappingURL=conditiondetail.component.js.map