import {Component, OnInit, DynamicComponentLoader} from 'angular2/core';
import {Condition}    from './condition';
import {DateCondition}    from './datecondition';
import {NgForm}    from 'angular2/common';
import {ConditionDetailComponent}    from './conditiondetail.component';
import {ConditionService} from 'app/condition.service'

@Component({
    selector: 'condition-builder',
    templateUrl: 'app/conditionbuilder.component.html',
    providers: [ConditionService],
    directives: [ConditionDetailComponent]
})

export class ConditionBuilderComponent implements OnInit {
    conditions: Condition[] = [];
    catalog: Condition[] = [];

    constructor(private _conditionService: ConditionService, private dcl: DynamicComponentLoader) { }

    getConditions() {
        this._conditionService.getConditions().then(conditions => this.catalog = conditions);
    }

    ngOnInit() {
        this.getConditions();
    }

    newCondition() {
        console.log('add');
        this.conditions.push(new DateCondition);

        console.log(this.conditions);
    }
}

