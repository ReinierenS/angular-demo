import {Condition} from './condition'
import {CONDITIONS} from './mock-arrays';
import {Injectable} from 'angular2/core';

@Injectable()
export class ConditionService {
    getConditions() {
        return Promise.resolve(CONDITIONS);
    }
}
