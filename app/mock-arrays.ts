import {Condition} from './condition';
import {DateCondition} from './datecondition'

export var CONDITIONS: Condition[] = [
    {"condition": "Auto"},
    {"condition": "Fiets"},
    {"condition": "Boot"},
    {"condition": "Vliegtuig"}
];

