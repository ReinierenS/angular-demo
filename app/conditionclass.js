System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Condition;
    return {
        setters:[],
        execute: function() {
            Condition = (function () {
                function Condition(condition) {
                    this.condition = condition;
                }
                return Condition;
            }());
            exports_1("Condition", Condition);
        }
    }
});
//# sourceMappingURL=conditionclass.js.map