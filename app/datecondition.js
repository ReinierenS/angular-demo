System.register(['angular2/core', './condition'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, condition_1;
    var DateTimeFormat, DateCondition;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (condition_1_1) {
                condition_1 = condition_1_1;
            }],
        execute: function() {
            DateCondition = (function (_super) {
                __extends(DateCondition, _super);
                function DateCondition() {
                    _super.apply(this, arguments);
                    this.name = 'date';
                }
                DateCondition = __decorate([
                    core_1.Component({
                        selector: 'condition-detail',
                        template: "\n    <div class=\"col-sm-3\">\n        <div class=\"form-group\">\n            operator\n        </div>\n    </div>\n    <div class=\"col-sm-3\">\n        <div class=\"form-group\">\n            <input [(ngModel)]=\"condition.value\" />\n        </div>\n    </div>\n    "
                    }), 
                    __metadata('design:paramtypes', [])
                ], DateCondition);
                return DateCondition;
            }(condition_1.Condition));
            exports_1("DateCondition", DateCondition);
        }
    }
});
//# sourceMappingURL=datecondition.js.map