
import {
    Component,
    OnInit,
    Input,
    DynamicComponentLoader,
    ElementRef,
    Attribute} from 'angular2/core';
import {Condition}    from './condition';
import {DateCondition}    from './datecondition';
import {NgForm, CORE_DIRECTIVES}    from 'angular2/common';

class MyComponentLoader {
    loadComponentConfig(url){
        return fetch(url)
            .then(res => res.json())
            .then(componentList =>
                Promise.all(componentList.map(config =>
                    this.loadComponent(config))))
    }
    loadComponent(configObject){
        return System.import(configObject.path)
            .then(componentModule =>
                componentModule[configObject.component])
    }
}

@Component({
    selector: 'condition-detail',
    bindings: [MyComponentLoader],
    directives: [CORE_DIRECTIVES],
    template: `
    <div id="content"></div>
    `
})

export class ConditionDetailComponent  {
    constructor(
        myLoader: MyComponentLoader,
        loader: DynamicComponentLoader,
        elementRef:ElementRef,
        @Attribute('src') configPath) {
        myLoader.loadComponentConfig(configPath)
            .then(components =>
                Promise.all(components.map(comp =>
                    loader.loadIntoLocation(comp, elementRef, 'content'))));
    }
}

